const express = require("express");
const app = express();
const http = require("http");
const mongoose = require("mongoose");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const port = process.env.PORT || 3000;
const authRoutes = require("./routes/auth");
const bodyParser = require("body-parser");

io.on("connection", (socket) => {
  console.log("socket connected");
  io.emit("chat message", {
    userName: process.env.PORT ? "*** Server HEROKU" : "*** Server localhost",
    msg: "Successfully connected to me!",
  });
  socket.on("chat message", (msg) => {
    io.emit("chat message", msg);
  });
});

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use(authRoutes);

app.use((err, req, res, next) => {
  console.log(err);
  const status = err.statusCode || 500;
  const message = err.message;
  const data = err.data;
  res.status(status).json({ message: message, data: data });
});

mongoose
  .connect(
    process.env.MONGODB_URI ||
      "mongodb+srv://roman:r1278983@cluster0.hafpw.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("==> connected DB!!!");
    let port_number = server.listen(port, () =>
      console.log("server running on port: " + port)
    );
  })
  .catch(() => console.log("err"));
