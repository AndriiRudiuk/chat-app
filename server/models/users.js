const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  user: {
    type: String,
    require: true,
  },
});
module.exports = mongoose.model("user", userSchema);
