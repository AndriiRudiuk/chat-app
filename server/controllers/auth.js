const User = require("../models/users");
const { validationResult } = require("express-validator");

exports.getPost = (req, res, next) => {
  res.status(200).json({ name: "Roma" });
};

exports.createUser = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation error", errors.array());
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const newUser = req.body.user;

  const user = new User({
    user: newUser,
  });
  user
    .save()
    .then(() => {
      console.log("added to BD");
      res.status(201).json({
        message: "user has been created",
        user: newUser,
      });
    })
    .catch((err) => console.log("user has NOT been created", err));
};

exports.login = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error("Validation error", errors.array());
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }
  const user = req.body.user;

  res.status(200).json({
    message: "user entred",
    user: user,
  });
};
