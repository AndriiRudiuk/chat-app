const express = require("express");
const user = require("../models/users");
const { body } = require("express-validator");
const feedController = require("../controllers/auth");
const router = express.Router();

// router.get("/post", feedController.getPost);
router.post(
  "/adduser",
  [
    body("user")
      .trim()
      .not()
      .isEmpty()
      .isLength({ min: 4 })
      .custom((value, { req }) => {
        return user.findOne({ user: value }).then((userDoc) => {
          if (userDoc) {
            return Promise.reject("alreay exist");
          }
        });
      }),
  ],
  feedController.createUser
);

router.post(
  "/login",
  [
    body("user")
      .trim()
      .not()
      .isEmpty()
      .isLength({ min: 4 })
      .custom((value, { req }) => {
        return user.findOne({ user: value }).then((userDoc) => {
          if (!userDoc) {
            return Promise.reject(`user not found in DB`);
          }
        });
      }),
  ],
  feedController.login
);

module.exports = router;
