import styled from "styled-components/native";
import { Platform } from "react-native";

export const Container = styled.View`
  width: 100%;
  border-color: #ddd;
  border-width: 1px;
  border-radius: 5px;
`;

export const Input = styled.TextInput`
  height: 35px;
  width: 100%;
  padding: 4px;
`;
