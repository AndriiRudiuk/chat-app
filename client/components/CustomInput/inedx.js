import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import { Container, Input } from "./style";

export const CustomInput = ({ placeholder = "", field, data, setData }) => {
  const setDataHandler = (text, field) => {
    setData({ ...data, [field]: text });
  };

  return (
    <Container>
      <Input
        placeholder={placeholder}
        onChangeText={(text) => setDataHandler(text, field)}
        value={data[field]}
      />
    </Container>
  );
};
