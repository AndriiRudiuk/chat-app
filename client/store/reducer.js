export const SET_MESSAGE = "SET_MESSAGE";

export const initialState = {
  messages: [],
};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_MESSAGE:
      return { ...state, messages: [...state.messages, payload] };

    default:
      return state;
  }
};
