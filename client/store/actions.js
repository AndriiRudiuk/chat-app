import { SET_MESSAGE } from "./reducer";

const setMessageAction = (data) => {
  return { type: SET_MESSAGE, payload: data };
};

// ----

export const setMessage = (data) => (dispatch) => {
  return dispatch(setMessageAction(data));
};
