import styled from "styled-components/native";
import { Platform } from "react-native";

export const Container = styled.View`
  background-color: #775f45;
  height: ${Platform.OS === "web" ? "60%" : "100%"};
  width: ${Platform.OS === "web" ? "40%" : "100%"};
  justify-content: center;
  align-items: center;
`;

export const InputWrapper = styled.View`
  justify-content: space-evenly;
  height: 20%;
  align-items: center;
  width: 70%;
`;

export const EnterButton = styled.TouchableOpacity`
  background-color: chocolate;
  border-width: 2px;
  border-color: #ddd;
  height: 40px;
  width: 70%;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
`;
