import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Platform,
  TextInput,
} from "react-native";
import { CustomInput } from "../../components";
import { Container, InputWrapper, EnterButton } from "./style.js";
const Login = () => {
  const [data, setData] = useState({ user: "", password: "" });

  return (
    <Container>
      <Text>SingIn</Text>
      <InputWrapper>
        <CustomInput
          field="user"
          data={data}
          setData={setData}
          placeholder="user"
        />
        <CustomInput
          field="password"
          data={data}
          setData={setData}
          placeholder="password"
        />
      </InputWrapper>

      <EnterButton onPress={() => console.log("press")}>
        <Text>SingIn</Text>
      </EnterButton>
    </Container>
  );
};

export default Login;

const styles = StyleSheet.create({
  inputWrapper: {
    justifyContent: "space-evenly",
    height: "10%",
    alignItems: "center",
  },
});
