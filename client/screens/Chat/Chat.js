import React, { useState, useEffect } from "react";
import { StyleSheet, Text, FlatList, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import io from "socket.io-client";
import { CustomInput } from "../../components";
import { setMessage } from "../../store/actions";
import { URL } from "../../constants";
import {
  Container,
  ChatView,
  ChatInput,
  EnterButton,
  AuthContainer,
  Login,
  SingUp,
  InputUserName,
  SubmitBtn,
  InputUserNameContainer,
} from "./style.js";

const Chat = () => {
  const dispatch = useDispatch();
  const messages = useSelector((state) => state.messages);
  const flatList = React.useRef(null);
  const [sock, setSocket] = useState();
  const [data, setData] = useState("");
  const [dataUserInput, setDataUserInput] = useState("");
  const [userInput, setUserInput] = useState(false);
  const [singUpMode, setSingUpMode] = useState(true);
  const [userName, setUserName] = useState("Anonim");
  const [isLogined, setLogined] = useState(false);

  let socket = null;

  useEffect(() => {
    socket = io(URL, {
      transports: ["websocket"],
      jsonp: false,
    });
    setSocket(socket);
    //socket.connect();
    socket.on("connect", () => {
      console.log("==> socket connected !!!");
    });

    socket.on("chat message", (msg) => {
      addToChat(msg, userName);
    });
    return () => {
      socket.disconnect();
      console.log("==> socket DISconnected !!!");
      setSocket(null);
    };
  }, []);

  const addToChat = (msg) => {
    setMessage({
      user: msg.userName,
      body: msg.msg,
      id: Math.random().toString(),
    })(dispatch);
  };
  const submitChatMessage = (msg, userName) => {
    if (sock) {
      sock?.emit("chat message", { msg, userName });
      setData("");
    }
  };

  const openSingUp = () => {
    setUserInput(true);
    setSingUpMode(true);
  };

  const openLogin = () => {
    if (isLogined) {
      sock?.emit("chat message", {
        userName: "* Admin",
        msg: `User ${userName} has left the chat`,
      });
      setLogined(false);
      setUserName("Anonim");
      setData("");
      setDataUserInput("");
    } else {
      setUserInput(true);
      setSingUpMode(false);
    }
  };

  const submit = () => {
    if (singUpMode) {
      axios
        .post(URL + "/adduser", {
          user: dataUserInput,
        })

        .then((res) => {
          if (sock) {
            sock?.emit("chat message", {
              userName: "* Admin",
              msg: `New user ${dataUserInput} has been created. You can log in with this name.`,
            });
            setData("");
            setDataUserInput("");
            setSingUpMode(false);
          }
        })
        .catch((err) => {
          const errors = err.response.data.data.map((i) => i.msg);
          console.log("err!!!", errors);
          if (errors.includes("Invalid value")) {
            sock?.emit("chat message", {
              userName: "* Admin",
              msg: `ERROR: Please enter a valid name!`,
            });
            setData("");
            setDataUserInput("");
          }
          if (errors.includes("alreay exist")) {
            sock?.emit("chat message", {
              userName: "* Admin",
              msg: `ERROR: This user is already exist!`,
            });
            setData("");
            setDataUserInput("");
          }
        });
    } else if (!isLogined) {
      if (userName !== "Anonim") {
        return;
      }
      axios
        .post(URL + "/login", {
          user: dataUserInput,
        })

        .then((res) => {
          if (sock) {
            sock?.emit("chat message", {
              userName: "* Admin",
              msg: `User ${dataUserInput} has been entred to Chat.`,
            });
            setLogined(true);
            setUserName(res.data.user);
            setData("");
            setUserInput(false);
            setDataUserInput("");
            setSingUpMode(true);
          }
        })
        .catch((err) => {
          const errors = err.response.data.data.map((i) => i.msg);
          console.log("err!!!", errors);
          if (errors.includes("user not found in DB")) {
            sock?.emit("chat message", {
              userName: "* Admin",
              msg: `ERROR: user not found`,
            });
            setData("");
            setUserName("Anonim");
            //setUserInput(false);
            setDataUserInput("");
            //setSingUpMode(true);
          } /*
          if (errors.includes("alreay exist")) {
            sock?.emit("chat message", `ERROR: This user is already exist!`);
            setData("");
            setUserInput(false);
            setDataUserInput("");
            setSingUpMode(true);
          }*/
        });
    } else {
      setUserName("Anonim");
      setLogined(false);
    }
  };

  return (
    <Container>
      <AuthContainer>
        <Login onPress={openLogin}>
          <Text>{!isLogined ? "LogIn" : "LogOut"}</Text>
        </Login>
        <SingUp onPress={openSingUp}>
          <Text> Create new user</Text>
        </SingUp>
      </AuthContainer>
      {userInput && (
        <InputUserNameContainer>
          <InputUserName
            onChangeText={(text) => setDataUserInput(text)}
            value={dataUserInput}
          />
          <SubmitBtn onPress={submit}>
            <Text>{singUpMode ? "SingUp" : "LogIn"}</Text>
          </SubmitBtn>
        </InputUserNameContainer>
      )}
      <ChatView>
        <FlatList
          ref={flatList}
          onContentSizeChange={() => {
            flatList.current.scrollToEnd({ animated: true });
          }}
          data={messages}
          renderItem={({ item }) => (
            <View>
              <Text style={{ fontWeight: "700" }}>{item.user}</Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: "#ccc",
                  padding: 5,
                  borderRadius: 5,
                  backgroundColor: "#ccc",
                }}
              >
                <Text style={{ flex: 0 }}>{item.body}</Text>
              </View>
            </View>
          )}
        />
      </ChatView>
      <ChatInput onChangeText={(text) => setData(text)} value={data} />
      <EnterButton
        onPress={() => {
          submitChatMessage(data, userName);
        }}
      >
        <Text>Send</Text>
      </EnterButton>
    </Container>
  );
};

export default Chat;
