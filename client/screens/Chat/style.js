import styled from "styled-components/native";
import { Platform } from "react-native";

export const Container = styled.View`
  background-color: #775f45;
  height: ${Platform.OS === "web" ? "60%" : "100%"};
  width: ${Platform.OS === "web" ? "40%" : "100%"};
  justify-content: flex-start;
  padding-top: 40px;
  align-items: center;
`;

export const ChatView = styled.View`
  height: 70%;
  width: 95%;
  background-color: white;
  justify-content: center;
  padding: 5px;
  border-radius: 8px;
`;

export const ChatInput = styled.TextInput`
  height: 10%;
  width: 95%;
  background-color: white;
  border-radius: 8px;
  margin-top: 15px;
  padding: 5px;
`;

export const EnterButton = styled.TouchableOpacity`
  background-color: chocolate;
  border-width: 2px;
  border-color: #ddd;
  height: 40px;
  width: 95%;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
`;

export const AuthContainer = styled.View`
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
  height: 40px;
  width: 95%;
`;

export const Login = styled.TouchableOpacity`
  margin: 5px;
  background-color: #ccc;
  flex: 1;
  text-align: center;
  padding: 5px;
`;
export const SingUp = styled.TouchableOpacity`
  margin: 5px;
  background-color: #ccc;
  flex: 1;
  text-align: center;
  padding: 5px;
`;

export const InputUserName = styled.TextInput`
  height: 30px;
  width: 50%;
  background-color: white;
  border-radius: 8px;
  margin: 0px 0 5px 0;
  padding: 5px;
`;

export const SubmitBtn = styled.TouchableOpacity`
  background-color: #ccc;
  height: 30px;
  text-align: center;
  padding: 5px;
  margin: 0 0 0 15px;
  width: 150px;
`;

export const InputUserNameContainer = styled.View`
  justify-content: center;

  flex-direction: row;
  height: 40px;
  width: 95%;
`;
