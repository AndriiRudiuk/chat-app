import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import { applyMiddleware, compose, createStore } from "redux";
import Login from "./screens/Login/Login";
import Chat from "./screens/Chat/Chat";
import { reducer } from "./store/reducer";
const store = createStore(reducer);
export default function App() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <Chat />
        <StatusBar style="auto" />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
